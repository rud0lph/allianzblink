## Instalación

* 1.- Copiar link para clonar
* 3.- ionic plugin add pegar/link....


## Desinstalar

1.- ionic plugin remove io-mariachi-blink

## Método

1 = Sonido blink
0 = Sin sonido

blinkplugin.blinkMethod('1', funcionOk, funcionError);

## Funcion ejemplo


```
#!JavaScript

function funcionOk(response)
{
	//TODO aquí hacer lo que sea con el base 64, ejemplo:
	var datos = "data:image/jpg;base64," + response;
        document.getElementById('myImage').src = datos;
}

function funcionError(response)
{
	//TODO aquí muestra algún error como evento cancelado
}
```