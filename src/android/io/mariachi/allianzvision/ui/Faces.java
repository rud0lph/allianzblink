package io.mariachi.allianzvision.ui;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.media.AudioManager;
import android.media.ToneGenerator;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.os.AsyncTask;
import android.graphics.drawable.ColorDrawable;
import android.view.Window;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.vision.CameraSource;
import com.google.android.gms.vision.MultiProcessor;
import com.google.android.gms.vision.Tracker;
import com.google.android.gms.vision.face.Face;
import com.google.android.gms.vision.face.FaceDetector;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import io.mariachi.allianzvision.ui.cvision.CameraSourcePreview;
import io.mariachi.allianzvision.ui.cvision.GraphicOverlay;
import io.mariachi.allianzvision.utils.Utils;


public class Faces extends AppCompatActivity {
    private Dialog loadDialog;

    private static final String TAG = "FaceTracker";

    private CameraSource mCameraSource = null;

    private CameraSourcePreview mPreview;
    private GraphicOverlay mGraphicOverlay;

    private static final int RC_HANDLE_GMS = 9001;
    // permission request codes need to be < 256
    private static final int RC_HANDLE_CAMERA_PERM = 2;

    ImageView backImg;
    LinearLayout controles;
    LinearLayout comenzar;
    TextView textoEstado;
    TextView textoContador;
    ImageView btnAccept;
    ImageView btnRetake;

    boolean estado = false;
    boolean initPreview = false;
    boolean flag = true;

    boolean beeps;
    int pause;

    boolean blinking = false;

    ImageView btnChangeCamera;
    FaceDetector detector;
    Context context;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getResources().getIdentifier("activity_face", "layout", getPackageName()));
        context = getApplicationContext();
        loadDialog = new Dialog(Faces.this);
        loadDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        loadDialog.setCancelable(false);
        loadDialog.setContentView(getResources().getIdentifier("load_dialog_faces", "layout", getPackageName()));
        loadDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        mPreview = (CameraSourcePreview) findViewById(
                getResources().getIdentifier("preview", "id", getPackageName()));
        mGraphicOverlay = (GraphicOverlay) findViewById(
                getResources().getIdentifier("faceOverlay", "id", getPackageName()));

        comenzar = (LinearLayout) findViewById(
                getResources().getIdentifier("idLayoutComenzarBlink", "id", getPackageName()));
        controles = (LinearLayout) findViewById(
                getResources().getIdentifier("idLayoutControlesBlink", "id", getPackageName()));
        textoEstado = (TextView) findViewById(
                getResources().getIdentifier("idTxtOutputBlink", "id", getPackageName()));
        textoContador = (TextView) findViewById(
                getResources().getIdentifier("txtContador", "id", getPackageName()));
        btnAccept = (ImageView) findViewById(
                getResources().getIdentifier("btnAcceptBlink", "id", getPackageName()));
        btnRetake = (ImageView) findViewById(
                getResources().getIdentifier("btnRetakeBlink", "id", getPackageName()));
        btnChangeCamera = (ImageView) findViewById(
                getResources().getIdentifier("btn_change_camera", "id", getPackageName()));

        btnRetake.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                comenzar.setVisibility(View.VISIBLE);
                controles.setVisibility(View.GONE);
                btnAccept.setVisibility(View.GONE);
                btnRetake.setVisibility(View.INVISIBLE);

                controles.setBackgroundColor(Color.parseColor("#44000000"));
                textoEstado.setText(
                        "Parpadea al escuchar el tono\nal finalizar la cuenta regresiva.\n\nFavor de realizar esta operación en un lugar iluminado.");
            }
        });

        backImg = (ImageView) findViewById(
                getResources().getIdentifier("backFace", "id", getPackageName()));
        backImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        btnAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnAccept.setEnabled(false);
                showLoadDialog();
                new RotateImg(Utils.pictureFinal).execute();
            }
        });

        // Check for the camera permission before accessing the camera.  If the
        // permission is not granted yet, request permission.
        int rc = ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA);
        if (rc == PackageManager.PERMISSION_GRANTED) {
            createCameraSource();
        } else {
            requestCameraPermission();
        }

        btnChangeCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.e("CameraChange", "Its Facing " + mCameraSource.getCameraFacing());
                try{
                    mPreview.stop();
                    if (mCameraSource != null) {
                        mCameraSource.release();
                    }
                    detector.release();
                    detector = new FaceDetector.Builder(context)
                            .setClassificationType(FaceDetector.ALL_CLASSIFICATIONS)
                            .build();

                    detector.setProcessor(
                            new MultiProcessor.Builder(new GraphicFaceTrackerFactory())
                                    .build());
                    if (mCameraSource.getCameraFacing() == CameraSource.CAMERA_FACING_FRONT) {
                        mCameraSource = new CameraSource.Builder(context, detector)
                                .setFacing(CameraSource.CAMERA_FACING_BACK)
                                .setRequestedPreviewSize(1280, 720)
                                .setRequestedFps(40.0f)
                                .setAutoFocusEnabled(true)
                                .build();
                        Utils.facingBack = true;
                    } else {
                        mCameraSource = new CameraSource.Builder(context, detector)
                                .setFacing(CameraSource.CAMERA_FACING_FRONT)
                                .setRequestedPreviewSize(1280, 720)
                                .setRequestedFps(40.0f)
                                .setAutoFocusEnabled(true)
                                .build();
                        Utils.facingBack = false;
                    }
                    startCameraSource();
                }
                catch(Exception ex){
                    Log.e("ErrorCamera", "No se puede iniciar camara " + ex);
                }
            }
        });
        if (Utils.validaBlink)
        {
            btnChangeCamera.setVisibility(View.GONE);
            comenzar.setVisibility(View.GONE);
            controles.setVisibility(View.VISIBLE);
            controles.setBackgroundColor(Color.parseColor("#44000000"));
            textoEstado.setText("Debe parpadear al finalizar la cuenta regresiva");
            try {
                cuentaAtras();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private void showLoadDialog(){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (!loadDialog.isShowing()){
                    loadDialog.show();
                }
            }
        });
    }

    private void dimissLoadDialog(){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (loadDialog.isShowing()){
                    loadDialog.dismiss();
                }
            }
        });
    }

    /**
     * Handles the requesting of the camera permission.  This includes
     * showing a "Snackbar" message of why the permission is needed then
     * sending the request.
     */
    private void requestCameraPermission() {
        Log.w(TAG, "Camera permission is not granted. Requesting permission");

        final String[] permissions = new String[]{Manifest.permission.CAMERA};

        if (!ActivityCompat.shouldShowRequestPermissionRationale(this,
                Manifest.permission.CAMERA)) {
            ActivityCompat.requestPermissions(this, permissions, RC_HANDLE_CAMERA_PERM);
            return;
        }

        final Activity thisActivity = this;

        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ActivityCompat.requestPermissions(thisActivity, permissions,
                        RC_HANDLE_CAMERA_PERM);
            }
        };

        Snackbar.make(mGraphicOverlay, "Access to the camera is needed for detection",
                Snackbar.LENGTH_INDEFINITE)
                .setAction("OK", listener)
                .show();
    }

    /**
     * Creates and starts the camera.  Note that this uses a higher resolution in comparison
     * to other detection examples to enable the barcode detector to detect small barcodes
     * at long distances.
     */
    private void createCameraSource() {
        detector = new FaceDetector.Builder(context)
                .setClassificationType(FaceDetector.ALL_CLASSIFICATIONS)
                .build();

        detector.setProcessor(
                new MultiProcessor.Builder(new GraphicFaceTrackerFactory())
                        .build());

        if (!detector.isOperational()) {
            // Note: The first time that an app using face API is installed on a device, GMS will
            // download a native library to the device in order to do detection.  Usually this
            // completes before the app is run for the first time.  But if that download has not yet
            // completed, then the above call will not detect any faces.
            //
            // isOperational() can be used to check if the required native library is currently
            // available.  The detector will automatically become operational once the library
            // download completes on device.
            Log.w(TAG, "Face detector dependencies are not yet available.");
        }

        if (!Utils.facingBack)
        {
            mCameraSource = new CameraSource.Builder(context, detector)
                    .setFacing(CameraSource.CAMERA_FACING_FRONT)
                    .setRequestedPreviewSize(1280, 720)
                    .setRequestedFps(40.0f)
                    .setAutoFocusEnabled(true)
                    .build();
        }
        else
        {
            mCameraSource = new CameraSource.Builder(context, detector)
                    .setFacing(CameraSource.CAMERA_FACING_BACK)
                    .setRequestedPreviewSize(1280, 720)
                    .setRequestedFps(40.0f)
                    .setAutoFocusEnabled(true)
                    .build();
        }
    }

    /**
     * Restarts the camera.
     */
    @Override
    protected void onResume() {
        super.onResume();
        startCameraSource();
    }

    /**
     * Stops the camera.
     */
    @Override
    protected void onPause() {
        super.onPause();
        mPreview.stop();
    }

    /**
     * Releases the resources associated with the camera source, the associated detector, and the
     * rest of the processing pipeline.
     */
    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mCameraSource != null) {
            mCameraSource.release();
        }
        Utils.facingBack = true;
    }

    /**
     * Callback for the result from requesting permissions. This method
     * is invoked for every call on {@link #requestPermissions(String[], int)}.
     * <p>
     * <strong>Note:</strong> It is possible that the permissions request interaction
     * with the user is interrupted. In this case you will receive empty permissions
     * and results arrays which should be treated as a cancellation.
     * </p>
     *
     * @param requestCode  The request code passed in {@link #requestPermissions(String[], int)}.
     * @param permissions  The requested permissions. Never null.
     * @param grantResults The grant results for the corresponding permissions
     *                     which is either {@link PackageManager#PERMISSION_GRANTED}
     *                     or {@link PackageManager#PERMISSION_DENIED}. Never null.
     * @see #requestPermissions(String[], int)
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions,
            int[] grantResults) {
        if (requestCode != RC_HANDLE_CAMERA_PERM) {
            Log.d(TAG, "Got unexpected permission result: " + requestCode);
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
            return;
        }

        if (grantResults.length != 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            Log.d(TAG, "Camera permission granted - initialize the camera source");
            // we have permission, so create the camerasource
            createCameraSource();
            return;
        }

        Log.e(TAG, "Permission not granted: results len = " + grantResults.length +
                " Result code = " + (grantResults.length > 0 ? grantResults[0] : "(empty)"));

        DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                finish();
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Face Tracker Allianz")
                .setMessage(
                        "This application cannot run because it does not have the camera "
                                + "permission.  The application will now exit.")
                .setPositiveButton("OK", listener)
                .show();
    }

    //==============================================================================================
    // Camera Source Preview
    //==============================================================================================

    /**
     * Starts or restarts the camera source, if it exists.  If the camera source doesn't exist yet
     * (e.g., because onResume was called before the camera source was created), this will be called
     * again when the camera source is created.
     */
    private void startCameraSource() {

        // check that the device has play services available.
        int code = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(
                getApplicationContext());
        if (code != ConnectionResult.SUCCESS) {
            Dialog dlg =
                    GoogleApiAvailability.getInstance().getErrorDialog(this, code, RC_HANDLE_GMS);
            dlg.show();
        }

        if (mCameraSource != null) {
            try {
                mPreview.start(mCameraSource, mGraphicOverlay);
            } catch (IOException e) {
                Log.e(TAG, "Unable to start camera source.", e);
                mCameraSource.release();
                mCameraSource = null;
            }
        }
    }

    //==============================================================================================
    // Graphic Face Tracker
    //==============================================================================================

    /**
     * Factory for creating a face tracker to be associated with a new face.  The multiprocessor
     * uses this factory to create face trackers as needed -- one for each individual.
     */
    private class GraphicFaceTrackerFactory implements MultiProcessor.Factory<Face> {
        @Override
        public Tracker<Face> create(Face face) {
            return new GraphicFaceTracker(mGraphicOverlay);
        }
    }

    /**
     * Face tracker for each detected individual. This maintains a face graphic within the app's
     * associated face overlay.
     */
    private class GraphicFaceTracker extends Tracker<Face> {
        private GraphicOverlay mOverlay;
        private FaceGraphic mFaceGraphic;

        GraphicFaceTracker(GraphicOverlay overlay) {
            mOverlay = overlay;
            mFaceGraphic = new FaceGraphic(overlay);
        }

        /**
         * Start tracking the detected face instance within the face overlay.
         */
        @Override
        public void onNewItem(int faceId, Face item) {
            mFaceGraphic.setId(faceId);
        }

        /**
         * Update the position/characteristics of the face within the overlay.
         */
        @Override
        public void onUpdate(FaceDetector.Detections<Face> detectionResults, Face face) {
            if (initPreview) {
                mOverlay.add(mFaceGraphic);
                mFaceGraphic.updateFace(face);

                if (mFaceGraphic.blink() && flag) {
                    Log.w("Face", "El usuario parpadeo");
                    flag = false;
                    blinking = true;
                    Utils.blink = true;
                    Utils.validaBlink=false;

                }
            } else {
                mOverlay.remove(mFaceGraphic);
            }
        }

        /**
         * Hide the graphic when the corresponding face was not detected.  This can happen for
         * intermediate frames temporarily (e.g., if the face was momentarily blocked from
         * view).
         */
        @Override
        public void onMissing(FaceDetector.Detections<Face> detectionResults) {
            mOverlay.remove(mFaceGraphic);
        }

        /**
         * Called when the face is assumed to be gone for good. Remove the graphic annotation from
         * the overlay.
         */
        @Override
        public void onDone() {
            mOverlay.remove(mFaceGraphic);
        }
    }

    public void cuentaAtras() throws InterruptedException {
        estado = true;
        beeps = true;
        pause = 1000;
        new Thread(new Runnable() {
            @Override
            public void run() {
                int conta = 3;
                if (Utils.validaBlink)
                {
                    conta = Utils.validaConta;
                }
                while (estado) {
                    Log.e("Contador", "contar " + conta);
                    if (conta >= 0) {
                        final int finalConta = conta;
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                textoContador.setText("" + finalConta);
                            }
                        });
                    } else {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                textoContador.setText(" ");
                            }
                        });
                    }
                    conta--;
                    try {
                        Thread.sleep(pause);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    if (conta == 0) {
                        initPreview = true;
                    }
                    if (Utils.sound == 1)
                    {
                        if (conta <= 0)
                        {
                            if (beeps){
                                beeps = false;
                                try{
                                    ToneGenerator toneG = new ToneGenerator(AudioManager.STREAM_ALARM, 100);
                                    toneG.startTone(ToneGenerator.TONE_CDMA_PRESSHOLDKEY_LITE, 1500);
                                }catch(Exception e){
                                    
                                }
                                
                            }
                            else {
                                beeps = true;
                            }
                            pause = 2000;
                        }
                    }
                    if (conta < -4) {
                        //preview.setCounter(false);
                        estado = false;
                        initPreview = false;
                    }
                    if (!estado) {
                        if (blinking) {

                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    controles.setBackgroundColor(Color.parseColor("#5558E2C2"));
                                    btnAccept.setVisibility(View.VISIBLE);
                                    btnRetake.setVisibility(View.GONE);
                                    textoEstado.setText("Gracias.");
                                }
                            });
                            blinking = false;
                        } else {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Utils.validaBlink = false;
                                    btnChangeCamera.setVisibility(View.VISIBLE);
                                    btnAccept.setVisibility(View.GONE);
                                    btnRetake.setVisibility(View.VISIBLE);
                                    controles.setBackgroundColor(Color.parseColor("#55EB2C2C"));
                                    textoEstado.setText(
                                            "Algo salió mal, repite el proceso de nuevo, gracias.");
                                }
                            });
                        }
                    }
                    Utils.validaConta = conta;
                }

            }
        }).start();

    }

    public void comenzarBlink(View v) {
        mCameraSource.takePicture(new CameraSource.ShutterCallback() {
            @Override
            public void onShutter() {
                //MediaActionSound sound = new MediaActionSound();
                //sound.play(MediaActionSound.SHUTTER_CLICK);
            }
        }, new CameraSource.PictureCallback() {
            @Override
            public void onPictureTaken(byte[] bytes) {

                Utils.pictureFinal = bytes;
                Utils.pictureTaken = null;

            }
        });
        Utils.validaBlink = true;
        Utils.validaConta = 3;
        btnChangeCamera.setVisibility(View.GONE);
        comenzar.setVisibility(View.GONE);
        controles.setVisibility(View.VISIBLE);
        controles.setBackgroundColor(Color.parseColor("#44000000"));
        textoEstado.setText("Parpadea al escuchar el tono\nal finalizar la cuenta regresiva\n\nFavor de realizar esta operación en un lugar iluminado.");
        try {
            cuentaAtras();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public class RotateImg extends AsyncTask<Void, Void, Void>{
        private byte[] picture;
        private String TAG = "ROTATION_BLINK_IMAGE";
        private File file;
        private Bitmap bitmapFrame;

        public RotateImg(byte[] picture){
            this.picture = picture;
        }

        @Override
        protected void onPreExecute() {
            Log.e(TAG, "ON PRE EXCECUTE");
            file = new File(getCacheDir(), "picture.jpg");
            FileOutputStream os = null;
            try {
                file.createNewFile();
                os = new FileOutputStream(file);
                os.write(picture);
                os.close();
            } catch (IOException e) {
                Log.e(TAG, "Cannot write to " + file, e);
            } finally {
                if (os != null) {
                    try {
                        os.close();
                    } catch (IOException e) {
                        // Ignore
                        Log.e(TAG, "Error Exception OS", e);
                    }
                }
            }
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            Log.e(TAG, "DO IN BACKGROUND");
            bitmapFrame = decodeFile(new File(file.getPath()), 800);
            Matrix matrix = new Matrix();
            int orientation = getImageRotation(file.getPath());
            matrix.preRotate(orientation != -1 ? orientation : 0);
            bitmapFrame = Bitmap.createBitmap(bitmapFrame, 0, 0, bitmapFrame.getWidth(), bitmapFrame.getHeight(), matrix, true);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            Log.e(TAG, "ON POST EXECUTE");
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bitmapFrame.compress(Bitmap.CompressFormat.PNG, 100, stream);
            Utils.pictureFinal = stream.toByteArray();
            Utils.blink = true;
            dimissLoadDialog();
            finish();
            super.onPostExecute(aVoid);
        }

        public Bitmap decodeFile(File file, int requiredHeight) {
            try {
                // Decode image size
                BitmapFactory.Options o = new BitmapFactory.Options();
                o.inJustDecodeBounds = true;
                Bitmap bitmap = BitmapFactory.decodeStream(new FileInputStream(file), null, o);
                if (bitmap != null) {
                    return bitmap;
                }
                // Find the correct scale value. It should be the power of 2.
                int scale = 1;
                while (o.outWidth / scale / 2 >= requiredHeight &&
                        o.outHeight / scale / 2 >= requiredHeight) {
                    scale *= 2;
                }
                // Decode with inSampleSize
                BitmapFactory.Options o2 = new BitmapFactory.Options();
                o2.inSampleSize = scale;
                return BitmapFactory.decodeStream(new FileInputStream(file), null, o2);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
                return null;
            }
        }

        public int getImageRotation(String imagePath) {
            try {
                File imageFile = new File(imagePath);
                if (imageFile.exists()) {
                    ExifInterface exif = new ExifInterface(imageFile.getPath());
                    int rotation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
                    return exifToDegrees(rotation);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return -1;
        }

        private int exifToDegrees(int exifOrientation) {
            if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_90) {
                return 90;
            } else if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_180) {
                return 180;
            } else if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_270) {
                return 270;
            }
            return 0;
        }
    }

    private void log(String content) {
        Log.e("myLog", content);
    }
}