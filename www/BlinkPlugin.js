var exec = require('cordova/exec');

exports.blinkMethod = function(arg0, success, error) {
    exec(success, error, "BlinkPlugin", "blinkMethod", [arg0]);
};

exports.blinkMethodjs = function(arg0, success, error) {
  if (arg0 && typeof(arg0) === 'string' && arg0.length > 0) {
    success(arg0);
  } else {
    error('Empty message!');
  }
};
